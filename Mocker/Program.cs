﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Cowboy.Sockets;
using GLCommon;
using Newtonsoft.Json.Linq;

namespace Mocker
{
    class Program
    {
        private static int _port;
        private static string _ip;
        private static string _com;
        private static SerialPort _serialPort;
        private static TcpSocketClient _client;
        static void Main(string[] args)
        {
            LogTool.Init();
            InitSetting();
            InitSerialPort();
            InitTcp();
            while (true)
            {
                var t = Console.ReadLine();
                if (t == "exit")
                    break;
            }
        }

        private static void InitTcp()
        {
            IPAddress ipAddress = IPAddress.Parse(_ip);
            IPEndPoint ep = new IPEndPoint(ipAddress, _port);
            var config = new TcpSocketClientConfiguration();
            config.FrameBuilder = new LengthFieldBasedFrameBuilder(LengthField.FourBytes);
            _client = new TcpSocketClient(ep, config);
            _client.Connect();
            LogTool.WriteInfoLog("Tcp已连接");
            _client.ServerConnected += _client_ServerConnected;
            _client.ServerDataReceived += _client_ServerDataReceived;
            _client.ServerDisconnected += _client_ServerDisconnected;
        }

        //串口收数据
        private static void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var data = _serialPort.ReadLine();
            LogTool.WriteInfoLog($"收到串口数据：{data}");
            if (_client.Connected)
            {
                _client.Send(Encoding.UTF8.GetBytes(data));
            }
        }
       
        //tcp收数据
        private static void _client_ServerDataReceived(object sender, TcpServerDataReceivedEventArgs e)
        {
            var data = Encoding.UTF8.GetString(e.Data);
            LogTool.WriteInfoLog($"收到ds数据：{data}");
            if (!_serialPort.IsOpen)
                return;
            _serialPort.WriteLine(data == "#getSerial" ? $"\"{data}\"" : data);
            
        }

        private static void _client_ServerDisconnected(object sender, TcpServerDisconnectedEventArgs e)
        {
             LogTool.WriteInfoLog("Tcp已连接");
        }

        private static void _client_ServerConnected(object sender, TcpServerConnectedEventArgs e)
        {
            LogTool.WriteInfoLog("Tcp已断开");
        }

        private static void InitSetting()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = config.AppSettings.Settings;
            _ip = settings["ip"].Value;
            _com = settings["com"].Value;
            _port = int.Parse(settings["port"].Value);
        }

        private static void InitSerialPort()
        {
            _serialPort = new SerialPort(_com, 9600, Parity.None, 8, StopBits.One);
            _serialPort.Open();
            _serialPort.DataReceived += _serialPort_DataReceived;
            LogTool.WriteInfoLog("串口已连接");
        }

       
    }
}
